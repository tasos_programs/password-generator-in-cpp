
# Password Generator in C++
A simple password generator made in C++

# Password Generator in C++
A simple password generator made in C++
## Usage
This program must be run through a **terminal**. 
There are no command line arguments.
## HOW TO RUN
You'll need to compile this program yourself for now. You'll need g++ for this.
- use `g++ Password_Generator.cpp` to compile
- run with **./a.out** for Linux
- or **a.exe** for Windows
