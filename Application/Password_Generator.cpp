#include <iostream>
#include <string>

using namespace std;


bool CheckDigidsValidity(string length)
{
    // Check if any of the chars in input (length) are NOT numbers
    int digitsFound = 0;

    for (char c : length)
    {
        digitsFound += isdigit(c);
    }

    // if all of the chars are numbers, digitsFound should be the same as length.length()
    if (digitsFound == length.length())
        return true;

    return false;
}

int CheckSizeValidity(int length)
{
    // Checks if specified length is below 6 (minimum required).
    if (length < 6)
    {
        return -1;
    }
    return length;
}

int HandleInput(string length)
{
    if (length.empty()) // Enter was pressed
    {
        return 12;
    }
    else if (CheckDigidsValidity(length)) // Input (length) was only numbers, not chars
    {
        int lengthToInt = abs(stoi(length));
        return CheckSizeValidity(lengthToInt);
    }
    return -1; // None of the conditions were met, so there was a user error.
}

int GetPasswordLength()
{
    while (true)
    {
        string length;
        
        cout << "Enter the maximum length for the password.\nMinimum size is 6, leave empty for 12: "; // TODO: user can make len = 0 etc.
        getline(cin,length);

        int lengthToInt = HandleInput(length);

        // break condition
        if (lengthToInt != -1) // if no errors occured
        {
            return lengthToInt;
        }       
    }
}

char CreateRandomCharacter()
{
    // ascii 32 - 126 (from char '!' up to 94 above it.)
    char character = '!' + rand() % 94;
    return character; 
}

void GeneratePassword(int length)
{
    srand(time(nullptr)); // initialize rand
    string password;

    for (int i = 0; i < length; i++) // Create as many chars as specified by the user
    {
        password += CreateRandomCharacter();       
    }
    cout << "Your password is: " << password << endl;   
}

int main()
{
    // We need to get password length (default is 12)
    // TODO: and if user wants symbols (special characters) [not implemented yet]
    int length = GetPasswordLength();
    GeneratePassword(length);
}
